import {Game} from "engine/framework/system/game/Game";
import {TextureLoader} from "engine-api/resource/loader/TextureLoader";
import {JSONLoader} from "engine-api/resource/loader/JSONLoader";
import {Resource} from "engine-api/resource/Resource";
import {ComponentBuilder} from "engine-api/system/componentBuilder/ComponentBuilder";
import {StateEvent} from "engine/core/common/event/StateEvent";
import {Application} from "engine-api/system/application/Application";
import {Sprite} from "engine-api/controls/Sprite";
import {Drawer} from "engine-api/graphics/Drawer";
import {View} from "engine-api/system/componentModel/View";
import {Container} from "engine-api/system/componentModel/Container";
import {LayoutManager} from "engine-api/system/layout/LayoutManager";
import {ILayout} from "engine-api/system/layout/ILayout";
import {Layout} from "engine-api/system/layout/Layout";
import {LayoutStructure} from "engine-api/system/layout/types/LayoutStructure";
import {Orientation} from "engine/core/common/types/Orientation";
import {DimensionAndOrientation, IViewport} from "engine-api/system/display/IViewport";
import {Viewport} from "engine-api/system/display/Viewport";
import {Dimension} from "engine/core/platform/DimensionAdapter";
import {Component} from "engine-api/system/componentModel/Component";
import {IContainer} from "engine-api/system/componentModel/IContainer";
import {GameModel} from "./models/GameModel";
import {GameController} from "./controllers/GameController";
import {ServicesEnum} from "./consts/ServicesEnum";
import {GameView} from "./views/GameView";
import {Card} from "./views/components/Card/Card";
import {LifeCycleManager} from "engine-api/system/componentModel/lifeCycle/LifeCycleManager";
import {SimpleButton} from "./views/components/Button/SimpleButton";
import {Loader} from "engine-api/resource/loader/Loader";
import {IAssetSource} from "engine-api/resource/types/IAssetSource";
import {AssetType} from "engine-api/resource/loader/AssetType";
import {AtlasType, AudioFormat, TextureFormat} from "engine-api/resource/types/Formats";
import {Manifest} from "engine-api/resource/manifest/Manifest";
import {GetSpineManifest} from "./consts/spineAssets";
import {IStringProvider} from "engine-api/language/IStringProvider";
import {StringProvider} from "engine-api/language/StringsProvider";

export class ScratchGame extends Game {
  private _layoutManager: LayoutManager;

  constructor() {
    super('ScratchGame');
  }

  protected async _init() {
    await this._loadAssets();

    const stringProvider: IStringProvider = StringProvider.getInstance();
    stringProvider.setLanguage(Resource.getAsset('en'));

    await this._buildScene();

    await this._applyLayout();

    this._registerServices();

    new GameController();
  }
  private async _loadAssets(): Promise<void> {
    const textureLoader: TextureLoader = new TextureLoader();
    const jsonLoader: JSONLoader = new JSONLoader();
    const loader: Loader = new Loader();
    const {audioSources, metadata} = this._getSoundDataToLoad();
    const {sources: spineSources, metadata: spineMetadata} = this._getSpineLoadData();
    const {sources: skeletalSources, metadata: skeletalMetadata} = this._getSkeletalData();

    await Promise.all([
      textureLoader.downloadAndLoadToMemory('img/logo.png'),
      textureLoader.downloadAndLoadToMemory('img/logo_dgc.png'),
      textureLoader.downloadAndLoadToMemory('img/button_up.png'),
      textureLoader.downloadAndLoadToMemory('img/button_over.png'),
      textureLoader.downloadAndLoadToMemory('img/button_down.png'),
      textureLoader.downloadAndLoadToMemory('img/icon_0.png'),
      textureLoader.downloadAndLoadToMemory('img/icon_1.png'),
      textureLoader.downloadAndLoadToMemory('img/icon_2.png'),
      textureLoader.downloadAndLoadToMemory('img/icon_back.png'),
      textureLoader.downloadAndLoadToMemory('img/particle.png'),

      jsonLoader.downloadAndLoadToMemory('scenes/scene.json'),
      jsonLoader.downloadAndLoadToMemory('locales/en.json'),

      loader.downloadAndLoadToMemory('sfxSound', audioSources, metadata),
    ]);

    const manifest = new Manifest('spineManifest', GetSpineManifest());
    await manifest.initialize();
    await manifest.downloadAndLoadGroupToMemory('skeletalLayersDemo');
  }

  private async _applyLayout(): Promise<void> {
    const sceneDataObject: any = Resource.getAsset('scene');
    const layoutData: LayoutStructure = sceneDataObject.layout;
    const layout : ILayout = Layout.getInstance();
    const viewport: IViewport = Viewport.getInstance();
    this._layoutManager = new LayoutManager(layout);

    await layout.parseLayoutData(layoutData, viewport.orientation.value);

    viewport.dimensionsAndOrientation.addListener(({orientation, dimension}: DimensionAndOrientation): void => {
        this._updateLayout(orientation, dimension);
    });
  }

  private _updateLayout(orientation: Orientation, dimension: Dimension): void {
    const root: IContainer = Application.game.getChild('game');
    this._layoutManager.alignControl(root, dimension.width, dimension.height, orientation, true);
  }

  private async _buildScene(): Promise<void> {
    const sceneDataObject: any = Resource.getAsset('scene');

    const builder: ComponentBuilder = new ComponentBuilder();
    const fakeLoadedEmitter: StateEvent<boolean> = new StateEvent<boolean>(false);
    builder.buildScene(Application.game, sceneDataObject, fakeLoadedEmitter.asEventListener());
    fakeLoadedEmitter.emit(true);
  }

  private _registerServices(): void {
    Application.addService(ServicesEnum.GAME_MODEL, new GameModel());
  }

  private _getSoundDataToLoad() {
    const audioSources: IAssetSource[] = [
      {
        src: 'audio/sfx.mp3',
        type: 'spriteSound',
        embedded: false
      },
    ];
    audioSources.push({
      src: `audio/sfx.json`,
      type: 'application/json',
      embedded: false
    });
    const metadata = {
      type: AssetType.AUDIO_SPRITE,
      files: [
        { name: 'sfx.mp3', extension: AudioFormat.MP3 },
      ],
      idList: [
        'sfx'
      ]
    };

    return {audioSources, metadata};
  }

  private _getSpineLoadData() {
    const sources = [
      {
        src: `spine/spineboy.png`,
        type: 'image/png',
        embedded: false
      }, {
        src: `spine/spineboy.atlas`,
        type: 'application/text',
        embedded: false
      },
      {
        src: `spine/spineboy.json`,
        type: 'application/json',
        embedded: false
      }
    ];
    const metadata = {
      type: AssetType.TEXTURE_ATLAS,
      files: [
        { name: 'spineboy.png', extension: TextureFormat.PNG },
        { name: 'spineboy.atlas', extension: AtlasType.ATLAS }
      ],
      idList: ['']
    };

    return {
      sources, metadata
    };
  }

  private _getSkeletalData() {
    const sources = [
      {
        src: `spine/spineboy.json`,
        type: 'application/json',
        embedded: false
      }
    ];
    const metadata = {
      type: AssetType.SKELETAL_ASSET,
      files: [
        {name: 'spineboy.json', extension: AtlasType.JSON}
      ]
    };

    return {sources, metadata};
  }

  // TOOD: remove later
  protected _hackFunction(): void {
    const classesToNotTreeshake = [Sprite, Drawer, Text, View, Component, Container, GameView, Card, SimpleButton];
  }
}


