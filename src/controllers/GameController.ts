import {Inject} from "engine-api/system/serviceContainer/InjectDecorator";
import {GameModel} from "../models/GameModel";
import {ServicesEnum} from "../consts/ServicesEnum";
import {IContainer} from "engine-api/system/componentModel/IContainer";
import {Application} from "engine-api/system/application/Application";
import {IGameView} from "../views/IGameView";

export class GameController {
  @Inject(ServicesEnum.GAME_MODEL)
  private _gameModel: GameModel;

  private _gameView: IGameView;

  constructor() {
    this._init();
  }

  private _init(): void {
    this._initViews();

    this._addListeners();
  }

  private _addListeners(): void {
    this._gameView.onGameFinished.addListener(this._onGameFinished, this);
    this._gameView.buyCardEvent.addListener(this._onBuyCard, this);
  }

  private _onBuyCard(): void {
    this._startGame();
  }

  private _startGame(): void {
    const cardData: number[] = this._generateCardData();
    const win: number = Math.floor(Math.random() * 1000) + 100;

    this._gameView.startGame(cardData, win);
  }

  private _generateCardData(): number[] {
    const arr = new Array(9).fill(0);
    arr.forEach((value, index) => {
      arr[index] = Math.floor(Math.random() * 2);
    });

    return arr;
  }

  private _onGameFinished(): void {
  }

  private _initViews(): void {
    const game: IContainer = Application.game.getChild('game');

    this._gameView = game.getChild('gameView');
  }
}