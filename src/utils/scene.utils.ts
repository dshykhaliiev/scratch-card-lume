import { Logger } from 'engine-api/system';
import { IComponent } from 'engine-api/system/componentModel/IComponent';
import { Scene } from 'engine-api/system/game/Scene';

/**
 *
 * The scene bind decorator.
 * Use this decorator to bind control instances to scene variables. The component path id is used to lookup the component instance.
 * @param idOrPath The component id or path used to find component instance.
 */
export function Fetch(idOrPath: string): Function {
  return function (target: Scene, propertyKey: string): TypedPropertyDescriptor<unknown> {
    // function (target: any, key: string):
    if (!(target instanceof Scene)) {
      // throw new Error('Can only use "@Fetch" on scenes.');
      Logger.warn("Please re-enable the throw here. Also something that isn't a scene is using @fetch");
    }
    if (typeof target[propertyKey] === 'function') {
      throw new Error('Binding to functions are not allowed.');
    }
    if (idOrPath === undefined || idOrPath === null) {
      throw new Error('Please specify the binding id or path for the component');
    }

    function performFetch(instance: Scene): IComponent {
      const splits = idOrPath.split('.');
      let inner: any = null;
      for (let i = 0; i < splits.length; i++) {
        const id = splits[i];
        inner = !inner ? instance.getComponent(id) : inner.getComponent(id);
      }
      return inner;
    }

    const storageId = '__fetched_' + idOrPath;
    return {
      get: function (): object {
        // tslint:disable:no-invalid-this we want "this" to adapt to the context of the scene instance
        if (this[storageId] === undefined || this[storageId] === null) {
          this[storageId] = performFetch(this);
        }
        return this[storageId];
        // tslint:enable:no-invalid-this
      },
      set: function (): void {
        // tslint:disable-next-line:no-any
        const className = (<any>target).name || target.constructor.name;
        Logger.warn(`Set: ${className} => Setting a value of a Fetch decorated scene component is not allowed!`);
      },
      enumerable: true,
      configurable: false,
    };
  };
}