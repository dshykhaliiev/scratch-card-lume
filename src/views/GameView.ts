import {Container} from "engine-api/system/componentModel/Container";
import {IGameView} from "./IGameView";
import {RegisterClass} from "engine-api/system/registry/ClassRegistryDecorators";
import {IContainer} from "engine-api/system/componentModel/IContainer";
import {Fetch} from "../utils/scene.utils";
import {Card} from "./components/Card/Card";
import {IEvent, IEventListener} from "engine/framework/system/event/Events";
import {Event} from "engine/core/common/event/Event";
import {SimpleButton} from "./components/Button/SimpleButton";
import {Skeletal} from "engine-api/animation/skeletal/Skeletal";
import {LifeCyclePhase} from "engine-api/system/componentModel/lifeCycle/LifeCycleState";
import {IViewport} from "engine-api/system/display/IViewport";
import {Viewport} from "engine-api/system/display/Viewport";
import {Orientation} from "engine/core/common/types/Orientation";
import {Text} from "engine/framework/controls/Text";
import {ComponentProperties} from "engine-api/Types";
import {TextType} from "engine-api/controls/Text";
import {IStringProvider} from "engine-api/language/IStringProvider";
import {StringProvider} from "engine-api/language/StringsProvider";
import { Tween } from 'engine/framework/animation/tween/Tween';


@RegisterClass()
export class GameView extends Container implements IGameView {
  @Fetch('card')
  private _card: Card;

  @Fetch('buttonWrapper.buyBtn')
  private _buyBtn: SimpleButton;

  @Fetch('buttonWrapper.revealBtn')
  private _revealBtn: SimpleButton;

  private _stringProvider: IStringProvider = StringProvider.getInstance();
  private _winText: Text;
  private _buyCardEvent: IEvent = new Event();
  private _gameFinishedEvent: IEvent = new Event();
  private _boySpine: Skeletal;
  private _win: number = 0;

  constructor(parent: IContainer, id: string) {
    super(parent, id);
  }

  startGame(cardData: number[], win: number): void {
    this._card.startGame(cardData);
    this._win = win;

    this._revealBtn.visible = true;

    const tween: Tween = new Tween(
      this._winText, 500,
      {alpha: 1}, {alpha: 0}
    );
    tween.onCompleteEvent.addListener(() => {
      this._winText.visible = false;
    });
    tween.play();
  }

  get onGameFinished(): IEventListener {
    return this._gameFinishedEvent.asEventListener();
  }

  get buyCardEvent(): IEventListener {
    return this._buyCardEvent.asEventListener();
  }

  protected _init(): void {
    super._init();

    this._initButtons();

    this._addListeners();

    this._createSpineAnimation();

    this._createWinText();
  }

  private _addListeners(): void {
    this._card.cardCompleteEvent.addListener(this._finishGame, this);
    this._buyBtn.clickEvent.addListener(this._onBuyBtnClick, this);
    this._revealBtn.clickEvent.addListener(this._onRevealBtnClick, this);
  }

  private _finishGame(): void {
    this._gameFinishedEvent.emit();
    this._revealBtn.visible = false;

    this._showWin();
  }

  private _onBuyBtnClick(): void {
    this._buyCardEvent.emit();
  }

  private _onRevealBtnClick(): void {
    this._card.revealAll();

    this._finishGame();
  }

  private _initButtons(): void {
    this._buyBtn.onTexture = 'button_up';
    this._buyBtn.hoverTexture = 'button_over';
    this._buyBtn.downTexture = 'button_down';
    this._buyBtn.textureId = this._buyBtn.onTexture;
    this._buyBtn.label = this._stringProvider.getString('buy');

    this._revealBtn.onTexture = 'button_up';
    this._revealBtn.hoverTexture = 'button_over';
    this._revealBtn.downTexture = 'button_down';
    this._revealBtn.textureId = this._revealBtn.onTexture;
    this._revealBtn.visible = false;
    this._revealBtn.label = this._stringProvider.getString('reveal');
  }


  private _createSpineAnimation(): void {
    const boySpine = this.createComponent(Skeletal, 'spineboy', {
      skeletonResourceId: 'spineboy',
      scale: 0.5,
      defaultMix: 0
    });
    boySpine.init();

    const viewport: IViewport = Viewport.getInstance();
    viewport.orientation.addListener((orientation) => {
      if (orientation === Orientation.PORTRAIT) {
        boySpine.x = 500;
        boySpine.y = 1400;
      }
      else {
        boySpine.x = 150;
        boySpine.y = 500;
      }
    });

    this._boySpine = boySpine;

    boySpine.lifeState.addListener((lifeCyclePhase: LifeCyclePhase) => {
      if (lifeCyclePhase === LifeCyclePhase.READY) {
        // start playing automatically
        const boyLayer = boySpine.getLayer(0);
        boySpine.setAnimation(boyLayer.index, 'walk', true);
        boySpine.play();
      }
    });
  }

  private _showWin(): void {
    this._winText.text = this._stringProvider.getString('youWon', this._win);
    this._winText.visible = true;

    const tween: Tween = new Tween(
      this._winText, 800,
      {alpha: 0}, {alpha: 1}
    );
    tween.play();
  }

  private _createWinText(): void {
    const textProps: ComponentProperties<Text> = {
      text: 'You win',
      textType: TextType.CANVAS,
      z: 21,
      originX: 0.5,
      originY: 0.5,
      style: {
        fontSize: 60,
        fill: '0xFFFFFF'
      }
    };

    this._winText = this.createComponent(Text, `${this.id}_text`, textProps);
    Viewport.getInstance().orientation.addListener((orientation) => {
      const dimensions = Viewport.getInstance().dimensions.value;
      const yPos = orientation === Orientation.PORTRAIT ? 100 : dimensions.height - 130

      this._winText.x = dimensions.width / 2;
      this._winText.y = yPos;
    });
    this._winText.visible = false;
  }
}