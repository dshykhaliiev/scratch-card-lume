import {IContainer} from "engine-api/system/componentModel/IContainer";
import {IEventListener} from "engine-api/system/event/Events";

export interface IGameView extends IContainer {
  startGame(cardData:number[], win: number): void;
  get onGameFinished(): IEventListener;
  get buyCardEvent(): IEventListener;
}