import {IContainer} from "engine-api/system/componentModel/IContainer";
import {IEventListener} from "engine/framework/system/event/Events";

export interface ICard extends IContainer {
  startGame(symbols: number[]): void;
  revealAll(): void;
  get cardCompleteEvent(): IEventListener;
}