import {IContainer} from "engine-api/system/componentModel/IContainer";
import {IEventListener} from "engine/framework/system/event/Events";

export interface ISymbol extends IContainer {
  setTextureId(id: string): void;
  setEnabled(value: boolean): void;
  reset(): void;
  reveal(): void;
  get isRevealed(): boolean;
  get revealedEvent(): IEventListener;
}