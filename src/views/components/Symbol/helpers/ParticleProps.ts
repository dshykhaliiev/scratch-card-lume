import {IEmitterProperties} from "engine-api/particle/IEmitterProperties";
import {IParticleFactory} from "engine/core/subsystems/particles/IParticleFactory";

export function GetParticleProps(factory: IParticleFactory) : IEmitterProperties {
  return {
    "sourcePosition": {
      "value": {
        "x": 0,
        "y": 0
      },
      "variance": {
        "x": 0,
        "y": 0
      }
    },
    "speed": {
      "value": 1,
      "variance": 0.5
    },
    "particleLifeSpan": {
      "value": 1,
      "variance": 0
    },
    "angle": {
      "value": 0,
      "variance": 360
    },
    "gravity": {
      "x": 0,
      "y": 0
    },
    "radialAcceleration": {
      "value": 0,
      "variance": 0.5
    },
    "tangentialAcceleration": {
      "value": 0,
      "variance": 0
    },
    "alpha": {
      "start": {
        "value": 1,
        "variance": 0
      },
      "finish": {
        "value": 0,
        "variance": 0
      }
    },
    "particleSize": {
      "start": {
        "value": 1.7,
        "variance": 0.3
      },
      "finish": {
        "value": 2,
        "variance": 0
      }
    },
    "duration": -1,

    "damping": 0.0075,
    "particlesPerEmission": {
      "min": 5,
      "max": 10
    },
    "delayBetweenEmission": {
      "min": 0.1,
      "max": 0.25
    },
    id: 'particles',
    particleFactory: factory,
  }
};